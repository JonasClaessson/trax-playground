import numpy
import types

def rtype(var):
    rtype_helper(0, var)

def print_with_indent(indent, s):
    print(f"{'  ' * indent}{s}")

def rtype_helper(indent, var):
    t = type(var)
    if isinstance(var, tuple):
        print_with_indent(indent, type(var))
        if len(var) > 0:
            for i in var:
                rtype_helper(indent + 1, i)
        else:
            rtype_helper(indent + 1, None)
    elif isinstance(var, list):
        print_with_indent(indent, f"{type(var)}")
        if len(var) > 0:
            rtype_helper(indent + 1, var[0])
        else:
            rtype_helper(indent + 1, None)
    elif isinstance(var, types.GeneratorType):
        print_with_indent(indent, f"{type(var)}")
        rtype_helper(indent + 1, next(var))
    elif isinstance(var, types.FunctionType):
        print_with_indent(indent, f"{type(var)} name: {var.__qualname__}, module: {var.__module__}")
    elif isinstance(var, numpy.ndarray):
        print_with_indent(indent, f"{type(var)} shape: {var.shape}")
    elif var == None:
        print_with_indent(indent, "None")
    else:
        print_with_indent(indent, type(var))
