# trax-playground

Testing out Trax ML library

## REPL
How to develop in the python repl.

```
$ /usr/local/Cellar/python@3.8/3.8.7/bin/python3.8
Python 3.8.7 (default, Dec 30 2020, 10:13:08)
[Clang 12.0.0 (clang-1200.0.32.28)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> exec(open("summary.py").read())
>>> next(inputs())
```
