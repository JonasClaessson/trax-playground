FROM ubuntu:20.10

RUN apt-get -y update &&\
        apt-get -y upgrade &&\
        apt-get install -y less wget git build-essential make cmake python3-pip &&\
        ln -s /usr/bin/python3 /usr/bin/python

RUN git clone https://github.com/google/trax.git

RUN cd trax &&\
        env TF_VERSION="2.3.*" TRAX_TEST="lib" TRAX_TEST="research" bash ./oss_scripts/oss_pip_install.sh &&\
        pip install tokenizers
