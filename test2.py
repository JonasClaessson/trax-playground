import sys
import os
import numpy as np
import textwrap
import trax
from trax import layers as tl
from trax.fastmath import numpy as jnp
from trax.supervised import training

wrapper = textwrap.TextWrapper(width = 70)

# to print the entire np array
np.set_printoptions(threshold=sys.maxsize)

# Special tokens
SEP = 0 # Separator token
EOS = 1 # End of sentence token

# Configure the vocabulary
vocab_dir  = 'vocab_dir/'
vocab_file = 'summarize32k.subword.subwords'

# Module configuration
module_dir = "~/model"

# Bucketing to create batched generators.
boundaries = [128, 256,  512, 1024]
batch_sizes = [16, 8, 4, 2, 1]

# Create the transformer model
def create_model(mode = 'train'):
    print('Create the transformer model\n')
    return trax.models.Transformer(
        input_vocab_size = 33300,
        d_model = 512,
        d_ff = 2048,
        n_heads = 8,
        n_encoder_layers = 6,
        n_decoder_layers = 6,
        max_len = 2048,
        mode=mode)

def load_data():
    print('Load the data\n')
    # Importing CNN/DailyMail articles dataset
    train_stream_fn = trax.data.TFDS('bool_q',
                                     data_dir='data/',
                                     keys=('question', 'answer'),
                                     train=True)

    # This should be much faster as the data is downloaded already.
    eval_stream_fn = trax.data.TFDS('bool_q',
                                    data_dir='data/',
                                    keys=('question', 'answer'),
                                    train=False)

    return [train_stream_fn, eval_stream_fn]

# Concatenate tokenized inputs and targets using 0 as separator.
def preprocess(stream):
    for (article, summary) in stream:
        joint = np.array(list(article) + [EOS, SEP] + list(summary) + [EOS])
        mask = [0] * (len(list(article)) + 2) + [1] * (len(list(summary)) + 1) # Accounting for EOS and SEP
        yield joint, joint, np.array(mask)

def create_data_streams():
    [train_stream_fn, eval_stream_fn] = load_data()

    # You can combine a few data preprocessing steps into a pipeline like this.
    input_pipeline = trax.data.Serial(
        # Tokenizes
        trax.data.Tokenize(vocab_dir=vocab_dir, vocab_file=vocab_file),
        # Uses function defined above
        preprocess,
        # Filters out examples longer than 2048
        trax.data.FilterByLength(2048)
    )

    # Apply preprocessing to data streams.
    train_stream = input_pipeline(train_stream_fn())
    eval_stream = input_pipeline(eval_stream_fn())

    # Create the streams.
    train_batch_stream = trax.data.BucketByLength(boundaries, batch_sizes)(train_stream)
    eval_batch_stream = trax.data.BucketByLength(boundaries, batch_sizes)(eval_stream)

    return [train_batch_stream, eval_batch_stream]

def tokenize(input_str, EOS=1):
    """Input str to features dict, ready for inference"""

    # Use the trax.data.tokenize method. It takes streams and returns streams,
    # we get around it by making a 1-element stream with `iter`.
    inputs =  next(trax.data.tokenize(iter([input_str]),
                                      vocab_dir=vocab_dir,
                                      vocab_file=vocab_file))

    # Mark the end of the sentence with EOS
    return list(inputs) + [EOS]

def detokenize(integers):
    """List of ints to str"""

    s = trax.data.detokenize(integers, vocab_dir=vocab_dir, vocab_file=vocab_file)

    return wrapper.fill(s)


def training_loop(TransformerLM, train_gen, eval_gen, output_dir = module_dir):
    '''
    Input:
        TransformerLM (trax.layers.combinators.Serial): The model you are building.
        train_gen (generator): Training stream of data.
        eval_gen (generator): Evaluation stream of data.
        output_dir (str): folder to save your file.

    Returns:
        trax.supervised.training.Loop: Training loop.
    '''
    output_dir = os.path.expanduser(output_dir)  # trainer is an object
    lr_schedule = trax.lr.warmup_and_rsqrt_decay(n_warmup_steps = 1000, max_value = 0.01)

    train_task = training.TrainTask(
        labeled_data = train_gen, # The training generator
        loss_layer = tl.CrossEntropyLoss(), # Loss function
        optimizer = trax.optimizers.Adam(0.01), # Optimizer
        lr_schedule = lr_schedule,
        n_steps_per_checkpoint = 10
    )

    eval_task = training.EvalTask(
        labeled_data = eval_gen, # The evaluation generator
        metrics = [tl.CrossEntropyLoss(), tl.Accuracy()] # CrossEntropyLoss and Accuracy
    )

    loop = training.Loop(TransformerLM,
                         train_task,
                         eval_tasks = [eval_task],
                         output_dir = output_dir)

    return loop

def next_symbol(cur_output_tokens, model):
    """Returns the next symbol for a given sentence.

    Args:
        cur_output_tokens (list): tokenized sentence with EOS and PAD tokens at the end.
        model (trax.layers.combinators.Serial): The transformer model.

    Returns:
        int: tokenized symbol.
    """

    # current output tokens length
    token_length = len(cur_output_tokens)

    # calculate the minimum power of 2 big enough to store token_length
    padded_length = 2**int(np.ceil(np.log2(token_length + 1)))

    # Fill cur_output_tokens with 0's until it reaches padded_length
    padded = cur_output_tokens + [0] * (padded_length - token_length)
    padded_with_batch = np.array(padded)[None, :]

    # model expects a tuple containing two padded tensors (with batch)
    output, _ = model((padded_with_batch, padded_with_batch))
    log_probs = output[0, token_length, :]

    return int(np.argmax(log_probs))

# Decoding functions.
def greedy_decode(input_sentence, model):
    """Greedy decode function.

    Args:
        input_sentence (string): a sentence or article.
        model (trax.layers.combinators.Serial): Transformer model.

    Returns:
        string: summary of the input.
    """

    # Use tokenize()
    cur_output_tokens = tokenize(input_sentence) + [0]
    generated_output = []
    cur_output = 0

    while cur_output != EOS:
        # Get next symbol
        cur_output = next_symbol(cur_output_tokens, model)
        # Append next symbol to original sentence
        cur_output_tokens.append(cur_output)
        # Append next symbol to generated sentence
        generated_output.append(cur_output)
        print(detokenize(generated_output))

    return detokenize(generated_output)

# Create the data streams
[train_batch_stream, eval_batch_stream] = create_data_streams()

# Train the model
loop = training_loop(create_model(mode = 'train'), train_batch_stream, eval_batch_stream)
loop.run(10)

# Test the model
test_sentence = "It was a sunny day when I went to the market to buy some flowers. But I only found roses, not tulips."
print(wrapper.fill(test_sentence), '\n')
print(greedy_decode(test_sentence, model))
