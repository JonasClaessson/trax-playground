import os
import trax
from trax import data as td
from trax import layers as tl
from trax import models as tm
from trax.fastmath import numpy as jnp
from trax.supervised import training

# Special tokens
SEP = 0 # Separator token
EOS = 1 # End of sentence token

# Configure the vocabulary
vocab_dir  = 'vocab_dir/'
vocab_file = 'summarize32k.subword.subwords'

# Module configuration
module_dir = 'model/'

# Bucketing to create batched generators.
boundaries = [128, 256,  512, 1024]
batch_sizes = [16, 8, 4, 2, 1]
max_length = max(boundaries)

# Convert the boolean values in the input stream to bytes
def toBytes(stream):
    for ex in stream:
        yield ( ex[0]
              , ex[1]
              , b'True' if ex[2] else b'False'
              )

# Concatenate the tokenized inputs and targets.
# The output will be a tensor of 'passage' EOS SEP 'question' EOS SEP 'answer' EOS
def concat(stream):
    for (passage, question, answer) in stream:
        joint = jnp.array(list(passage) + [EOS, SEP] + list(question) + [EOS, SEP] + list(answer) + [EOS])
        mask = [0] * (len(list(passage)) + len(list(question)) + 4) + [1] * (len(list(answer)) + 1)
        yield joint, joint, jnp.array(mask)

# Evaluation data generator
def eval_generator():
    gen_func = td.Serial(
        td.TFDS('bool_q', keys = ('passage', 'question', 'answer'), train = 'validation')
    )
    return gen_func()

# Data generator
def data_generator(train = True):
    gen_func = td.Serial(
        td.TFDS('bool_q', keys = ('passage', 'question', 'answer'), train = train),
        toBytes,
        td.Tokenize(vocab_dir = vocab_dir, vocab_file = vocab_file),
        concat,
        td.Shuffle(),
        td.FilterByLength(max_length = max_length, length_keys = [0]),
        td.BucketByLength(boundaries = boundaries,
                          batch_sizes = batch_sizes,
                          length_keys = [0]),
    )
    return gen_func()

# Create the transformer model
def create_model(mode = 'train'):
    print('Create the transformer model')
    return tm.Transformer(
        input_vocab_size = 33300,
        d_model = 512,
        d_ff = 2048,
        n_heads = 8,
        n_encoder_layers = 6,
        n_decoder_layers = 6,
        max_len = max_length,
        mode = mode)

def training_loop(TransformerLM, train_gen, eval_gen, output_dir = module_dir):
    '''
    Input:
        TransformerLM (trax.layers.combinators.Serial): The model you are building.
        train_gen (generator): Training stream of data.
        eval_gen (generator): Evaluation stream of data.
        output_dir (str): folder to save your file.

    Returns:
        trax.supervised.training.Loop: Training loop.
    '''
    output_dir = os.path.expanduser(output_dir)  # trainer is an object
    lr_schedule = trax.lr.warmup_and_rsqrt_decay(n_warmup_steps = 1000, max_value = 0.01)

    train_task = training.TrainTask(
        labeled_data = train_gen, # The training generator
        loss_layer = tl.CrossEntropyLoss(), # Loss function
        optimizer = trax.optimizers.Adam(0.01), # Optimizer
        lr_schedule = lr_schedule,
        n_steps_per_checkpoint = 10)

    eval_task = training.EvalTask(
        labeled_data = eval_gen, # The evaluation generator
        metrics = [tl.CrossEntropyLoss(), tl.Accuracy()]) # CrossEntropyLoss and Accuracy

    loop = training.Loop(
        TransformerLM,
        train_task,
        eval_tasks = [eval_task],
        output_dir = output_dir)

    return loop

# Train the model
def train_model():
    model = create_model()
    train_loop = training_loop(model, data_generator(), data_generator(train = False))
    train_loop.run(1)
    return model

# Load the model from the filesystem
def load_model():
    # The load model may not work yet
    predict_signature = trax.shapes.ShapeDtype((1,1), dtype=np.int32)
    model = create_model(mode = 'predict')
    model.init_from_file(module_dir + 'model.pkl.gz', weights_only = True, input_signature = predict_signature)
    return model

# Pad input up to the next power of 2 length
def pad(integers):
    token_length = len(integers)
    padded_length = 2**int(jnp.ceil(jnp.log2(token_length)))
    padded = integers + [0] * (padded_length - token_length)
    return padded

def tokenize(passage, question):
    # Tokenize the input
    tok_passage = next(td.tokenize(
        iter([passage]),
        vocab_dir = vocab_dir,
        vocab_file = vocab_file))
    tok_question = next(td.tokenize(
        iter([question]),
        vocab_dir = vocab_dir,
        vocab_file = vocab_file))
    tokenized = list(tok_passage) + [EOS, SEP] + list(tok_question) + [EOS, SEP]
    tokenized = pad(tokenized)

    # Decode from the Transformer
    tokenized = jnp.array(tokenized)[None, :]  # Add batch dimension
    return tokenized

def detokenize(integers):
    # De-tokenize answer
    tokenized_answer = integers[0][:-1]  # Remove batch and EOS
    answer = td.detokenize(
        tokenized_answer,
        vocab_dir = vocab_dir,
        vocab_file = vocab_file)
    return answer

def predict(model, passage, question):
    tokenized_input = tokenize(passage, question)
    tokenized_output = trax.supervised.decoding.autoregressive_sample(
        model,
        inputs = tokenized_input,
        temperature = 0.0,
        max_length = 3)
    answer = detokenize(tokenized_output)
    return answer

# Generate one sample of evaluation data
ed = next(eval_generator())
passage = ed[0]
question = ed[1]

# This works but load_module doesn't
m = train_model()
inp = tokenize(passage, question)
m((inp, inp))

# Do a test prediction
predict(m, passage, question)
